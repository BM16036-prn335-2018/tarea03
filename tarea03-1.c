#include <stdio.h>

int main()
{
	/* code */

	int menu;
	int *PunteroMenu;
	int suma=0;
	int *punteroSuma;
	int resta=0;
	int *punteroResta;
	int num1=0;
	int *punteroNum1;
	int num2=0;
	int *punteroNum2;
	int verificador=0;
	int *punteroVerificador;

	do
	{
		/* Muestra al usuario el menu de opciones */
	    printf("\n=======================MENU===========================\n");
		printf("Ingrese una opcion del menu\n");
		printf("1-ingresar dos numeros enteros\n");
		printf("2-calcular la suma de los dos numeros enteros\n");
		printf("3-calcular la resta de los dos numeros enteros\n");
		printf("4-inprimir la direccion de memoria de cada variable\n");
		printf("...............Otro numero para salir..............\n");
		printf("======================================================\n");

		scanf("%d",&menu);

		if (menu==1)
		{
			/* code */
			printf("Ingrese el valor del primer numero\n");
			scanf("%d",&num1);
			printf("Ingrese el valor del segundo numero\n");
			scanf("%d",&num2);

			verificador=1;

		}else if (menu==2)
		{
			/* code */
			if (verificador==0)
			{
				/* code */
				printf("No se a introducido ningun valor\n");

			}else if (verificador==1)
			{
				/* code */
				punteroNum1=&num1;
				punteroNum2=&num2;
				suma=*punteroNum1+*punteroNum2;
				printf("La suma de el valor que contiene la direccion de memoria:%d con el valor que contiene la direccion de memoria:%d es:%d\n", punteroNum1, punteroNum2, suma);

			}

		}else if (menu==3)
		{
			/* code */
			if (verificador==0)
			{
				/* code */
				printf("No se a introducido ningun valor\n");

			}else if (verificador==1)
			{
				/* code */
				punteroNum1=&num1;
				punteroNum2=&num2;
				suma=*punteroNum1-*punteroNum2;
				printf("La resta de el valor que contiene la direccion de memoria:%d con el valor que contiene la direccion de memoria:%d es:%d\n", punteroNum1, punteroNum2, suma);
				
			}

		}else if (menu==4)
		{
			/* code */
			printf("La direccionde memoria de la variable menu es:%d.\n",PunteroMenu);
			printf("La direccionde memoria de la variable suma es:%d.\n",punteroSuma);
			printf("La direccionde memoria de la variable resta es:%d.\n",punteroResta);
			printf("La direccionde memoria de la variable num1 es:%d.\n",punteroNum1);
			printf("La direccionde memoria de la variable num2 es:%d.\n",punteroNum2);
			printf("La direccionde memoria de la variable verificador es:%d.\n",punteroVerificador);
		}else
		{

		}
	} while (menu>0&&menu<5);

	return 0;
}