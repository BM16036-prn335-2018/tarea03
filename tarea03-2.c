#include <stdio.h>

int main()
{
	
int tamanio;/*variable que manejara el tamaño de los arrays*/

do
{
	/* se pide al usuario el ingrese del tamaño de los arrays dentro de un do while el cual verifica si el tamaño es menor si es asi lo pedira hasta que sea mayo a 0*/
	printf("Ingrese el tamanio de los arrays: \n");
	scanf("%d",&tamanio);

} while (tamanio<1);
	
int array1[tamanio];/*declaracion de un vector de tipo entero*/
int *punteroArray1;/*puntero que manejara los datos del array1*/
int array2[tamanio];/*declaracion de un vector de tipo entero*/
int *punteroArray2;/*puntero que manejara los datos del array1*/

	printf("LLene los arrays con datos.\n");


	/*Se pide el ingreso de datos del array 1 mediante un for*/
		for (int i = 0; i < tamanio; ++i)
	{
		printf("Ingrese el dato del array 1 en la posicion %d.\n", i+1);
		scanf("%d",&array1[i]);		
	}


	printf("===========================================================================================\n");

	/*Se pide el ingreso de datos del array 2 mediante un for*/
		for (int i = 0; i < tamanio; ++i)
	{
		printf("Ingrese el dato del array 2 en la posicion %d.\n", i+1);
		scanf("%d",&array2[i]);		
	}


	printf("===========================================================================================\n");
	/*muestra los datos ingresados en el array 1 al usuario*/
	printf("Los datos de los arrays son:\n");

		for (int i = 0; i < tamanio; ++i)
	{
		printf("%d\t",array1[i]);	
	}
		printf("\n");

	
	/*muestra los datos ingresados en el array 2 al usuario*/
		for (int i = 0; i < tamanio; ++i)
	{
		
		printf("%d\t",array2[i]);	
	}
		printf("\n");


	/* Se intercambian los datos del array1 con los datos del array 2 */
	    for (int i = 0; i < tamanio; ++i)
	{
	    	
	    	punteroArray1=&array1[i];/*Se guarda en punteroArray1 la direccion de memoria del array1 en la posiscion i*/
	    	punteroArray2=&array2[i];/*Se guarda en punteroArray2 la direccion de memoria del array2 en la posiscion i*/
	    	array1[i]=*punteroArray1+*punteroArray2;/*En la variable array1 se guarda la suma del valor que contiene el puntero punteroArray1 con el valor que contiene el puntero punteroArray2*/
	    	array2[i]=*punteroArray1-*punteroArray2;/*En la variable array2 se guarda la resta del valor que contiene el puntero punteroArray1 con el valor que contiene el puntero punteroArray2*/
	    	array1[i]=*punteroArray1-*punteroArray2;/*En la variable array1 se guarda la resta del valor que contiene el puntero punteroArray1 con el valor que contiene el puntero punteroArray2*/

    }
		
    printf("===========================================================================================\n");
		printf("Los datos de los arrays reordenados son:\n");
	/*Muestra al usuario el array1 con los datos que se le asignaron al array2*/	
		for (int i = 0; i < tamanio; ++i)
	{
		/* code */
		printf("%d\t",array1[i]);	
	}
		printf("\n");

	
	/*Muestra al usuario el array2 con los datos que se le asignaron al array1*/	
		for (int i = 0; i < tamanio; ++i)
	{
		/* code */
		printf("%d\t",array2[i]);	
	}
		printf("\n");	

	return 0;
}