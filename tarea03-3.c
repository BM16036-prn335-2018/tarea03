#include "stdio.h"

int main()
{
	int tamanio;/*variable que manejara el tamaño de los arrays*/

	do
    {
	/* se pide al usuario el ingrese del tamaño de los arrays dentro de un do while el cual verifica si el tamaño es menor si es asi lo pedira hasta que sea mayo a 0*/
	printf("Ingrese el tamanio del array: \n");
	scanf("%d",&tamanio);
    } while (tamanio<1);


	int array[tamanio];/*declaracion de un vector de tipo entero*/
	int *punteroArray;/*puntero que manejara los datos del array*/

	printf("LLene el arrays con datos.\n");


	/*Se pide el ingreso de datos del array  mediante un for*/
	for (int i = 0; i < tamanio; ++i)
	{
	printf("Ingrese el dato del array en la posicion %d.\n", i+1);
	scanf("%d",&array[i]);		
	}

	printf("===========================================================================================\n");
	/*muestra los datos ingresados en el array al usuario*/
	printf("Los datos del array son:\n");

	for (int i = 0; i < tamanio; ++i)
	{
	printf("%d\t",array[i]);	
	}
	printf("\n");

	printf("===========================================================================================\n");

	/*muestra los datos ingresados en el array impresos al reves al usuario*/
	printf("Los datos del array impreso al reves son:\n");

		for (int i = tamanio-1; i >= 0; --i)
	{
		punteroArray=&array[i];
		printf("%d\t",*punteroArray);	
	}
		printf("\n");
	return 0;
}